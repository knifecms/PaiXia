package com.robot.controller;

import com.jfinal.core.Controller;

/**
 * Created by smith on 2016/12/24.
 */
public class IndexController extends Controller {
    public void index() {
        renderText("Hello JFinal World.");
    }
}
