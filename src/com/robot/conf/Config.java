package com.robot.conf;

import com.jfinal.config.*;
import com.robot.controller.IndexController;

/**
 * Created by smith on 2016/12/24.
 */
public class Config extends JFinalConfig{

        public void configConstant(Constants me) {
            me.setDevMode(true);
        }
        public void configRoute(Routes me) {
            me.add("/hello", IndexController.class);
        }
        public void configPlugin(Plugins me) {}
        public void configInterceptor(Interceptors me) {}
        public void configHandler(Handlers me) {}
}
